Pro využití route, zadané pomocí řetězce "{area:exists}/{controller=Products}/{action=Index}/{id?}":

Jak se programově zajistí správné routování do Controlleru v jiné Area?

Pokud chceme uchovávat stav služby zaregistrované v kontaineru Autofacu, jaký typ registrace je vhodný?

MVC je zkratka pro?

Které z následujících tvrzení o Controlleru je pravdivé?

Které z následujících tvrzení o Action metodách je pravdivé?

Jaká je defaultní HTTP metoda pro každou action metodu?

K čemu se využívají Areas v ASP.NET MVC?

Jakým znakem se odlišuje syntax Razeru?

Co obsahují soubory cshtml?

Kterou registraci DI typu použijeme, chceme-li, aby existovala jedna jediná instance, vytvořena pro všechny závislosti?

Kterou registraci DI typu použijeme, chceme-li, aby byla během jednoho requestu vytvořena jedna jediná instance třídy bez ohledu na počet závislostí?

Kterou registraci DI typu použijeme, chceme-li, aby byla během jednoho requestu třída vytvořena tolikrát, kolikrát je její závislost vyžádována?

K čemu slouží třída HtmlHelper?

Co je hlavním úkolem Dependency Injection?

Jak se rozliší dvě action metody se stejným názvem pro různé Http metody?

K čemu slouží Controller v architektonickém typu MVC?

K čemu slouží Model v architektonickém typu MVC?

K čemu slouží View v architektonickém typu MVC?

Která část architektonického typu MVC obsluhuje interakci (požadavky) uživatele?

Která část architektonického typu MVC prezentuje uživateli obsah pomocí uživatelského rozhraní?

Která část architektonického typu MVC obsahuje stav, data a business logiku komponent v aplikaci?

Ve kterém souboru nalezneme definici pro routování v ASP.NET Core aplikaci?

Které z následujících není metodou implementace Dependency Injection?

Jak se zapisuje víceřádkový blok razer kódu?

Jakým klíčovým slovem označíme v deklaraci metodu, v jejímž těle budeme chtít alespoň jednou použít klíčové slovo await?

Jaký význám má modifikátor 'static' v deklaraci třídy v c#?

Jaký access modifikátor typu použijeme, jestliže chceme, aby byl typ přístupný kdekoliv v Assembly a nebo assembly, které na něj má odkaz?

Jaký access modifikátor typu použijeme, jestliže chceme, aby byl typ přístupný pouze ve stejné třídě nebo struktuře a nebo třídě odvozené (dědění)?

Jaký access modifikátor typu použijeme, jestliže chceme, aby byl typ přístupný pouze ve stejném assembly?

Jaký access modifikátor typu použijeme, jestliže chceme, aby byl typ přístupný pouze ve stejném assembly nebo odvozené třídě v jiném assembly?

Jaký access modifikátor typu použijeme, jestliže chceme, aby byl typ přístupný pouze ve stejné třídě?