﻿using System;
using System.Collections.Generic;
using System.Text;

namespace utb_eshop_patek.Application.Client.ViewModels.Carts
{
    public class IndexViewModel
    {
        public IList<CartItemViewModel> CartItems { get; set; }
        public decimal Total { get; set; }
    }
}
