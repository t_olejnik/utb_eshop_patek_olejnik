﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace utb_eshop_patek.Infrastructure.Identity.Roles
{
    public class Role : IdentityRole<int>
    {
        public Role(string name) : base(name)
        {
        }
    }
}
