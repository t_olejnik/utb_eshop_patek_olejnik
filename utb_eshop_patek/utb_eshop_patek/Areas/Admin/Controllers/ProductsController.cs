﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using utb_eshop_patek.Application.Admin.ApplicationServices.Products;
using utb_eshop_patek.Application.Admin.ViewModels.Products;
using utb_eshop_patek.Areas.Admin.Controllers.Common;

namespace utb_eshop_patek.Areas.Admin.Controllers
{
    public class ProductsController : AdminController
    {
        private readonly IProductApplicationService _productApplicationService;

        public ProductsController(IProductApplicationService productApplicationService)
        {
            _productApplicationService = productApplicationService;
        }

        public IActionResult Index()
        {
            var vm = _productApplicationService.GetIndexViewModel();
            return View(vm);
        }

        public IActionResult Edit(int id)
        {
            var product = _productApplicationService.GetProductViewModel(id);
            return View(product);
        }

        [HttpPost]
        public IActionResult Edit(ProductViewModel model)
        {
            var product = _productApplicationService.Update(model);
            return Json(product);
        }

        public IActionResult Insert()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Insert(ProductViewModel model)
        {
            var product = _productApplicationService.Insert(model);
            return Json(product);
        }

        public IActionResult Delete(ProductViewModel model)
        {
            _productApplicationService.Delete(model);
            return RedirectToAction(nameof(Index));
        }
    }
}