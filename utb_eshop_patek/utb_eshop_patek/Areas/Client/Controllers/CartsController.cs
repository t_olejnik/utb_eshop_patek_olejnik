﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using utb_eshop_patek.Application.Client.ApplicationServices.Carts;
using utb_eshop_patek.Domain.Constants;

namespace utb_eshop_patek.Areas.Client.Controllers
{
    [Area("Client")]
    public class CartsController : Controller
    {
        private readonly ICartApplicationService _cartApplicationService;

        public CartsController(ICartApplicationService cartApplicationService)
        {
            _cartApplicationService = cartApplicationService;
        }

        public IActionResult Index()
        {
            var vm = _cartApplicationService.GetIndexViewModel(GetTrackingCode());
            return View(vm);
        }

        [HttpPost]
        public JsonResult AddToCart(int id, int amount)
        {
            var cartItem = _cartApplicationService.AddToCart(id, amount, GetTrackingCode());
            return Json(cartItem);
        }

        [HttpPost]
        public JsonResult RemoveFromCart(int id)
        {
            _cartApplicationService.RemoveFromCart(id, GetTrackingCode());
            return Json("success");
        }

        private string GetTrackingCode()
        {
            var utc = Request.Cookies[Cookies.UserTrackingCode];
            if (utc != null)
                return utc;

            var guid = Guid.NewGuid().ToString();
            var options = new CookieOptions();
            options.Expires = DateTime.Now.AddYears(1);

            Response.Cookies.Append(Cookies.UserTrackingCode, guid, options);
            return guid;
        }
    }
}