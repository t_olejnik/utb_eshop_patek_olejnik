﻿using Microsoft.AspNetCore.Mvc;
using utb_eshop_patek.Application.Admin.ApplicationServices.Products;

namespace utb_eshop_patek.Areas.Client.Controllers
{
    [Area("Client")]
    public class ProductsController : Controller
    {
        private readonly IProductApplicationService _productApplicationService;

        public ProductsController(IProductApplicationService productApplicationService)
        {
            _productApplicationService = productApplicationService;
        }

        public IActionResult Index()
        {
            var vm = _productApplicationService.GetIndexViewModel();
            return View(vm);
        }
        public IActionResult Detail(int id)
        {
            var vm = _productApplicationService.GetProductViewModel(id);
            return View(vm);
        }
    }
}